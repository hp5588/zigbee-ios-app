//
//  ActivityIndicator.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/25.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ActivityIndicator :  NVActivityIndicatorViewable{
    static let size : Int = 100
    static let shared = ActivityIndicator()
    static private var view : UIView?
    static var nvActivityIndicator : NVActivityIndicatorView!
    static var fullView : UIView?

    static var active : Bool = false

    private init() {
//        var nvActivityIndicator : NVActivityIndicatorView!
//        ActivityIndicator.nvActivityIndicator = NVActivityIndicatorView(frame: CGRect(x: 10, y: 10, width: ActivityIndicator.size, height: ActivityIndicator.size))
//        ActivityIndicator.nvActivityIndicator.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.3 )
//        ActivityIndicator.nvActivityIndicator.padding = CGFloat(ActivityIndicator.size/5)
//        ActivityIndicator.nvActivityIndicator.layer.cornerRadius = CGFloat(ActivityIndicator.size/5)
    }
    

    

    static func setView(view : UIView){
        ActivityIndicator.nvActivityIndicator = NVActivityIndicatorView(frame:
            CGRect(x: Int(view.center.x) - ActivityIndicator.size/2,
                   y: Int(view.center.y) - ActivityIndicator.size/2,
                   width: Int(ActivityIndicator.size),
                   height: Int(ActivityIndicator.size)
            )
        )
        fullView = UIView(frame: (view.frame))
        ActivityIndicator.nvActivityIndicator.backgroundColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.3 )
        ActivityIndicator.nvActivityIndicator.padding = CGFloat(ActivityIndicator.size/5)
        ActivityIndicator.nvActivityIndicator.layer.cornerRadius =
            CGFloat(ActivityIndicator.size/5)
        ActivityIndicator.nvActivityIndicator.type = .orbit

        
        ActivityIndicator.view = view
    }
    
    
    
    
    static func show(title : String? = nil, timeout: Int? = nil){
        if !active {
            fullView?.backgroundColor = UIColor(hexString: "#00000040")
            ActivityIndicator.nvActivityIndicator.startAnimating()
            ActivityIndicator.view?.addSubview(fullView!)
            ActivityIndicator.view?.addSubview(nvActivityIndicator)
            

            active = true
            
            //timeout timer
            if timeout != nil {
                DispatchQueue.main.async {
                    Timer.scheduledTimer(withTimeInterval: TimeInterval(timeout!), repeats: false){
                        timer in
                        ActivityIndicator.dismiss()
                    }
                }
            }
        }

    }
    
    static func dismiss(){
        if active {
            ActivityIndicator.fullView?.removeFromSuperview()
            ActivityIndicator.nvActivityIndicator.removeFromSuperview()
            ActivityIndicator.nvActivityIndicator.stopAnimating()

            active = false
        }

    }
    
    

    
//    private static var shareIndicator: NVActivityIndicatorView = {
//        
//        return nvActivityIndicator
//    }()
//    
    

}
