//
//  AppDelegate.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/6.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import GoogleMaps
import GooglePlaces
import UserNotifications
import SwiftLocation



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //init data manager
        _ = DataManager.shared
        
        //set google map API key
        GMSServices.provideAPIKey("AIzaSyCHZIQdyluB_4A6tw6JHB0Ucb0FC5cefjc")
        GMSPlacesClient.provideAPIKey("AIzaSyCHZIQdyluB_4A6tw6JHB0Ucb0FC5cefjc")
        
        //customize activity indicator
        setupActivityIndicator();
        
        //ask right for notification
        NotificationManager.shared.requestAuth()
        
        //setup region monitor and read data
        RegionMonitor.shared.monitorDeviecs(deviceList: DataManager.shared.getDeviceList(shouldRetreive: true))
        CLLocationManager().startMonitoringSignificantLocationChanges()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    }


}

extension AppDelegate {
    
    func setupActivityIndicator() {
        //setup function
//        let data = ActivityData(size: CGSize.init(width: 80, height: 80), messageFont: UIFont.boldSystemFont(ofSize: 15), type: NVActivityIndicatorType.ballGridPulse, color: UIColor.blue, padding: 10, minimumDisplayTime: 1, backgroundColor: UIColor.black)
//        
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(data)
//        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()


    }
    
}

