//
//  CellBase.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/12.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit




class CellBase: UITableViewCell {
    
    var device : DeviceModel! {
        didSet{
            updateCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 8
        self.layer.shadowOpacity = 0.25
        self.layer.masksToBounds = false;
        self.clipsToBounds = false;
        
        
//        self.backgroundView?.alpha = 0.5
        var color = self.backgroundColor
        self.backgroundColor =  color?.withAlphaComponent(0.8)
//        self.backgroundColor =  UIColor.black

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
    func updateCell() {

    }
    

}
