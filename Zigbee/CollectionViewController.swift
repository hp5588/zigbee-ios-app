//
//  CollectionViewController.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/6.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import Foundation
import UIKit

import CocoaMQTT
import NVActivityIndicatorView

class CollectionViewCotroller: UICollectionViewController {
    
    let rootTopic = "/tdc/water-tank"
    let commandTopic = "command"
    let size : CGFloat = 100
    var mqtt : CocoaMQTT!
//    var nvActivityIndicator : NVActivityIndicatorView!
//    var activityIndicator : ActivityIndicator!
    var deviceList = [DeviceModel]()
    let dataManager = DataManager.shared

    
    var selectedDeviceIndex : Int! = 0
    var detailController : DetailController!
    var settingController : SettingTableViewController!
    var window: UIWindow?

    override func viewDidLoad() {
        //customize navigation bar
        self.navigationItem.title = "所有裝置"
        self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 120)

        //init mqtt client
        initMqtt()
        
        //init activity indicator
        _ = ActivityIndicator.shared
        ActivityIndicator.setView(view: view)
        
        //read devices list
        deviceList = dataManager.getDeviceList(shouldRetreive: true)
        
        window = (UIApplication.shared.delegate as! AppDelegate).window

        //set UI application delegates
        UIApplication.shared.delegate = (self as UIApplicationDelegate)
        settingController.delegate = self

    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        if (mqtt.connState != CocoaMQTTConnState.connected){
            ActivityIndicator.show()
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deviceList.count;
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell :MyCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyCollectionViewCell
        let currentDevice = deviceList[indexPath.row]
//        cell.textLabel.text = currentDevice.name;
//        cell.subTextLabel.text = currentDevice.id
        cell.device = currentDevice
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.7).cgColor
        cell.layer.borderWidth = 5
        return cell;
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        selectedDeviceIndex = indexPath.row
//        selectedDevice = deviceList[indexPath.row]
        self.performSegue(withIdentifier: "show-detail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        (segue.destination as! DetailController).device = deviceList[selectedDeviceIndex]
        detailController = (segue.destination as! DetailController);
        detailController.delegate = self
    }
    
}


extension CollectionViewCotroller: UIApplicationDelegate{

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //won't be called
        return true
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {

    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        //TODO testing remove later
//        NotificationManager.shared.notifyInRange(device: deviceList[1])
        
        dataManager.setDeviceList(deviceList: deviceList, shouldSave: true)
        mqtt.disconnect()
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        deviceList =  dataManager.getDeviceList(shouldRetreive: true)
        mqtt.connect()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {

        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        dataManager.setDeviceList(deviceList: deviceList, shouldSave: true)
        mqtt.disconnect()
    }
    
}


extension CollectionViewCotroller:CocoaMQTTDelegate{
    //MQTT functions
    func initMqtt() {
        let clientID = "CocoaMQTT-" + String(ProcessInfo().processIdentifier)
        mqtt = CocoaMQTT(clientID: clientID, host: "odoo.acu-unit.com", port: 1883)
        mqtt.username = "homeassistant"
        mqtt.password = "lhp75312"
        mqtt.willMessage = CocoaMQTTWill(topic: "/will", message: "dieout")
        mqtt.keepAlive = 60
        mqtt.delegate = self
        mqtt.autoReconnect = true;
        mqtt.connect()
    }
    
    func mqtt(_ mqtt: CocoaMQTT, didConnect host: String, port: Int){
        //?
    }
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck){
        print("connected to " + mqtt.host + ":" + String(mqtt.port))
        
        //subscribe to topic
        mqtt.subscribe(rootTopic + "/list/#")
    }
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16){
        
    }
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16){
        
    }
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ){
        
        
        let topic = message.topic
        print("recieve message from" + topic + " with payload " + String(describing: message.payload))
        
        
        let topicItem = message.topic.components(separatedBy: "/")
        if (topic.contains("list")){
            let deviceId = topicItem.last
            
            let deviceIndex = deviceList.index(where:  {$0.id == deviceId})
            if deviceIndex != nil {
                //reconnected and update device
                let device : DeviceModel = deviceList[deviceIndex!]
                device.connected = true
      
                let payloadString = String(bytes: message.payload, encoding: String.Encoding.utf8)!
                device.modules = resolve(payloadString: payloadString)
                
                subscribeDeviceProperty(device: device)

                updateCellView(index: deviceIndex!)

            }else{
                //add new device
                let device : DeviceModel = DeviceModel(id: deviceId!)
                let payloadString = String(bytes: message.payload, encoding: String.Encoding.utf8)!
                device.modules = resolve(payloadString: payloadString)

                device.connected = true
                
                addNewDevice(device: device)
            }

//            nvActivityIndicator.stopAnimating();
            ActivityIndicator.dismiss()
            

            
        }else{
            //manage property change event
            let id = topicItem[topicItem.count-2]
            let module = DeviceModel.Modules(rawValue:topicItem[topicItem.count-1])
            let state = String(bytes: message.payload, encoding: String.Encoding.utf8)!
            
            
            let deviceIndex = deviceList.index(where: {$0.id == id})!
            let device  = deviceList[deviceIndex]
            
            //Power,Switch,Temperature,WaterLevel,WaterFlow
            switch module! {
            case .Power:
                let info = state.components(separatedBy: ",")
                device.property.powerVolt = Float (info[0])
                device.property.powerCurrent = Float (info[1])
                break;
            
            case .Switch:
                device.property.switchState = DeviceModel.Switch(rawValue: state)
                break;
            
            case .Temperature:
                device.property.temperature = Float(state)
                break
            
            case .WaterLevel:
                device.property.waterLevel = Float(state)
                break
                
            case .WaterFlow:
//                device.property.waterLevel = Int(state)
                break
            case .DoorLock:
                device.property.switchState = DeviceModel.Switch(rawValue: state)
                print("door unlcok state : " + String(describing: device.property.switchState))
                
                //clear activity indicator
                ActivityIndicator.dismiss()
                
                break
            }
            
            
            deviceList[deviceIndex] = device
            
            
            //update both detail and collection view cell
            updateCellView(index: deviceIndex)
        }
        
    }
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String){
        print("now subscribe from " + topic)
    }
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String){
        
    }
    func mqttDidPing(_ mqtt: CocoaMQTT){
        
    }
    func mqttDidReceivePong(_ mqtt: CocoaMQTT){
        
    }
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?){
        //        print("disconnected from " + mqtt.host + ":" + String(mqtt.port) + " with error " + err)t
        mqtt.connect()
        print("disconnected")
    }
    func mqtt(_ mqtt: CocoaMQTT, didReceive trust: SecTrust, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(true)
        print("Trust Info verified")
        
    }
    func mqtt(_ mqtt: CocoaMQTT, didPublishComplete id: UInt16) {
    }

}
extension CollectionViewCotroller : DetailDelegate{

    func deviceUpdated(device : DeviceModel) {
        updateCellView(index: deviceList.index(where: {$0.id == device.id})!)
    }

    func switchStateChanged(isOn: Bool) {
        let currentDevice = detailController.device
        let topic = rootTopic + "/" + currentDevice.id + "/" + DeviceModel.Modules.Switch.rawValue + "/" + commandTopic
        let state = (isOn) ? DeviceModel.Switch.On : DeviceModel.Switch.Off
        //send command to topic
        mqtt.publish(topic, withString: state.rawValue, qos: CocoaMQTTQOS.qos2 , retained: false, dup: false)
    }
    func doorUnlockButtonPressed(){
        //send unlock command to topic
        let currentDevice = detailController.device
        let topic = rootTopic + "/" + currentDevice.id + "/" + DeviceModel.Modules.DoorLock.rawValue + "/" + commandTopic
        let state = DeviceModel.Switch.On
        mqtt.publish(topic, withString: state.rawValue, qos: CocoaMQTTQOS.qos2 , retained: false, dup: false)
    }
    
    func shouldSaveData() {
        DataManager.shared.setDeviceList(deviceList: deviceList, shouldSave: true)
    }
    
}

extension CollectionViewCotroller : SettingActionDelegate{
    func restActionTriggered() {

        
        let alert  = ConfirmAlertController(title: "Reset Network", message: "Are you sure you want to RESET whole network?", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .destructive, handler: {(alertaction : UIAlertAction)-> Void in
            self.resetNetwork()
            self.dismiss(animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(alertaction : UIAlertAction)-> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)

    }
    
    func resetNetwork(){
        print("rest")
        deviceList.removeAll()
        dataManager.setDeviceList(deviceList: deviceList, shouldSave: true)
        collectionView?.reloadData()
        mqtt.disconnect()
        mqtt.connect()
        self.tabBarController?.selectedIndex = 0
//        self.navigationController?.popViewController(animated: true)
    }
}

extension CollectionViewCotroller{
    func updateCellView(index : Int) {
        //detail cell view
        if detailController != nil {
            detailController.device = deviceList[index]
        }
        
        //collection cell view
        let indexPath = IndexPath(row: index, section: 0)
        let cell = (collectionView?.cellForItem(at: indexPath) as! MyCollectionViewCell)
        cell.device = deviceList[index]
//        collectionView?.reloadData()
         ActivityIndicator.dismiss()
    }
    
    func addNewDevice(device:DeviceModel){
        //add to device list
        deviceList.append(device)
        
        //update view
        collectionView?.reloadData()
        
        //subscribe other property
        subscribeDeviceProperty(device: device)
    }
    
    func resolve(payloadString: String) -> Set<DeviceModel.Modules>{
        let modulesOnDevice = payloadString.components(separatedBy: ";")
        var modules : Set<DeviceModel.Modules> = Set()
  
        print("--module available on new device--")
        for module in modulesOnDevice {
            print(module)
            modules.insert(DeviceModel.Modules(rawValue: module)!)
//            device.modules.insert(DeviceModel.Modules(rawValue: module)!)
        }
        print("----------------------------------")
        return modules
        
    }
    
    func subscribeDeviceProperty(device:DeviceModel) {
        let deviceTopic = rootTopic + "/" + device.id + "/"
        
        for module in device.modules {
            mqtt.subscribe(deviceTopic + "\(module)")
        }
    }
    

    
}





