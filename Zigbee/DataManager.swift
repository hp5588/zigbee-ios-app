//
// Created by 高 伯文 on 2017/9/5.
// Copyright (c) 2017 高 伯文. All rights reserved.
//

import Foundation


class DataManager {
    
    static let shared = DataManager()
    private var deviceList : [DeviceModel] = []
    
    init() {
        retrieve()
    }

    //write to NSUserDefault
    func save() {
        let userDefault = UserDefaults.standard
        let nsdata = NSKeyedArchiver.archivedData(withRootObject: deviceList)
        userDefault.setValue(nsdata, forKey: "device-list")
        userDefault.synchronize()
    }

    //read from NSUserDefault 
    //(will overwrite unsaved devices list)
    func retrieve(){
        let userDefault = UserDefaults.standard
        if let data = userDefault.data(forKey: "device-list") {
            deviceList = NSKeyedUnarchiver.unarchiveObject(with: data) as! [DeviceModel]
        }
    }
    
    func setDeviceList(deviceList: [DeviceModel], shouldSave: Bool? = false) {
        self.deviceList = deviceList
        if (shouldSave)! {
            save()
        }
    }
    
    func getDeviceList(shouldRetreive: Bool? = false) -> [DeviceModel] {
        if shouldRetreive! {
            retrieve()
        }
        return deviceList
    }
    
//    func updateDevice(devcive : DeviceModel) -> Bool {
//        return true
//    }
    

}
