//
//  DetailController.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/8.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import LocalAuthentication
import GooglePlaces

protocol DetailDelegate{
    func deviceUpdated(device : DeviceModel)
    func switchStateChanged(isOn : Bool)
    func doorUnlockButtonPressed()
    func shouldSaveData()
}

class DetailController: UITableViewController ,NVActivityIndicatorViewable{
    
    private let powerCellIdentifier         = "detail-cell-power"
    private let waterlevelCellIdentifier    = "detail-cell-waterlevel"
    private let temperatureCellIdentifier   = "detail-cell-temperature"
    private let switchCellIdentifier        = "detail-cell-switch"
    private let doorlockCellIdentifier      = "detail-cell-doorlock"
    
    internal let mapSegueIdentifier          = "show-map"

    
    var delegate : DetailDelegate!
    
    var optionsAlert : UIAlertController = UIAlertController()
    var nameModificationAlert: UIAlertController! = UIAlertController()

//    private var itemCount = 5;
    
    
    
    var device : DeviceModel = DeviceModel(id: "default"){
        didSet{
            //trigger cell to update
            for cell in tableView.visibleCells {
                (cell as! CellBase).device = device
            }
            
            //update navigation title
            self.navigationItem.title = device.name
            

//            tableView.reloadData()
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //remove separator
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.edit, target: self, action: #selector(editButtonPressed))
        
//        let image = UIImage(named: "Background")
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "Background"))
        
        //init alert controllers
        initAlertControllers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let modules = device.modules
        switch modules![(modules!.index((modules!.startIndex), offsetBy: indexPath.row))] {
        case .WaterLevel:
            return 200
        case .DoorLock:
            
            return view.frame.height/3
        default:
            return 150
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return device.modules.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        var identifier:String!
        
        let modules = device.modules
        
        switch modules![(modules!.index((modules!.startIndex), offsetBy: indexPath.row))] {
        case .Power:
            identifier = powerCellIdentifier
            break
        case .Switch:
            identifier = switchCellIdentifier
            break
        case .Temperature:
            identifier = temperatureCellIdentifier
            break
        case .WaterFlow:
//                identifier = waterlevelCellIdentifier
            break
        case .WaterLevel:
            identifier = waterlevelCellIdentifier
            break
        case .DoorLock:
            identifier = doorlockCellIdentifier
            break
        }
        

        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)

        
        if cell.reuseIdentifier == switchCellIdentifier {
            (cell as! SwitchCell).stateSwitch.addTarget(self, action: #selector(switchHandle(mySwitch:)), for: UIControlEvents.valueChanged)
        }else if(cell.reuseIdentifier == doorlockCellIdentifier){
            (cell as! DoorLockCell).authorizeButton.addTarget(self, action: #selector(doorLockHandle(myButton:)), for:UIControlEvents.touchUpInside)
        }

        // Configure the cell...
        
        (cell as! CellBase).device = device
//        (cell as! CellBase).updateCell()


        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    
}



extension DetailController : UITextFieldDelegate{
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        nameModificationAlert.dismiss(animated: true, completion: {
            self.device.name = textField.text
            self.delegate.deviceUpdated(device: self.device)
        })
        return true
    }
}


extension DetailController {
    
    @objc func editButtonPressed() {
        present(optionsAlert, animated: true, completion: nil)
    }

}

extension DetailController {

    func initAlertControllers()  {
        //prepare alert view
        nameModificationAlert = UIAlertController(title: "Device Name", message: "enter the name of device", preferredStyle: UIAlertControllerStyle.alert)
        nameModificationAlert.addTextField{
            (textField : UITextField!) -> Void in
            textField.placeholder = "ex. Water-Tank-A1"
            textField.text = self.device.name
            textField.clearButtonMode = .whileEditing
            textField.keyboardAppearance = .dark
            textField.delegate = self
            textField.autocapitalizationType = .words
        }
        
        optionsAlert.addAction(UIAlertAction(title: "Change Device Name", style: .default, handler: {(alertaction : UIAlertAction)-> Void in
            self.optionsAlert.dismiss(animated: true, completion: nil)
            //show name modification alert view
            self.present(self.nameModificationAlert, animated: true, completion: nil)
        }))
        
        if device.modules.contains(.DoorLock) {
            optionsAlert.addAction(UIAlertAction(title: "Set Location", style: .default, handler: {(alertaction : UIAlertAction)-> Void in
                self.optionsAlert.dismiss(animated: true, completion: nil)
                //show map to set location
                self.performSegue(withIdentifier: self.mapSegueIdentifier, sender: self)
            }))
        }

        optionsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
    }
}


//cell callback
extension DetailController{
    func switchHandle(mySwitch: UISwitch){
        print("switch state:" + String(mySwitch.isOn))
        delegate.switchStateChanged(isOn: mySwitch.isOn)

    }
    func doorLockHandle(myButton : UIButton) {
        
        let context = LAContext();
        let myLocalizedReasonString = "To Unlock The Door Requires Your Authentication"
        var authError: NSError? = nil

        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { (success, evaluateError) in
                if (success) {
                    // User authenticated successfully, take appropriate action
                    //show loading indicator
                    DispatchQueue.main.async {
                        ActivityIndicator.setView(view: UIApplication.shared.keyWindow!)
                        ActivityIndicator.show( timeout: 5)
                    }
                    //inform delegate
                    self.delegate.doorUnlockButtonPressed();
                } else {
                    // User did not authenticate successfully, look at error and take appropriate action
                }
            }
        } else {
            print("no Biometrics devices available")
            // Could not evaluate policy; look at authError and present an appropriate message to user
        }
    }
}

//override
extension DetailController{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier ==  mapSegueIdentifier {
            (segue.destination as! MapViewController).delegate = self
            if device.location != nil {
                (segue.destination as! MapViewController).setInitLocation(location: device.location!)
            }
        }
    }
}


extension DetailController : MapViewDelegate{
    func mapViewWillReturn(location: CLLocationCoordinate2D) {
        device.location = location
        delegate.shouldSaveData()
        
        //update Location monitor
        RegionMonitor.shared.updateMonitorDeviceFromDataManager()
    }
}


extension DetailController : GMSAutocompleteViewControllerDelegate{
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

