//
//  DeviceModel.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/8.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit


class DeviceModel : NSObject, NSCoding{

    var name : String! = "unamed"
    var id : String!
    var modules : Set<Modules>! = Set()
    var property : Property = Property()
    var connected : Bool = false
    var location : CLLocationCoordinate2D? = CLLocationCoordinate2D(latitude: 23.051388, longitude: 120.176612)
        
    
    let KEY_NAME = "name"
    let KEY_ID = "id"
    let KEY_MODULES = "modules"
    let KEY_LOCATION = "location"
    let KEY_LOCATION_LONG = "location-long"
    let KEY_LOCATION_LATI = "location-lati"

    
    struct Property {
        var temperature : Float! = 0
        
        var waterFLow : Int! = 0
        var waterLevel : Float! = 0
        
        var powerVolt : Float! = 0
        var powerCurrent : Float! = 0
        
        var switchState : Switch! = .Off
    }
    

    enum Modules : String{
        case Temperature
        case WaterFlow
        case WaterLevel
        case Power
        case Switch
        case DoorLock
    }

    enum Switch : String{
        case Off
        case On
    }
    
    
    init(id:String) {
        self.id = id
    }
    
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: KEY_NAME)
        aCoder.encode(id, forKey: KEY_ID)
        if location != nil {
            aCoder.encode(location?.latitude, forKey: KEY_LOCATION_LATI)
            aCoder.encode(location?.longitude, forKey: KEY_LOCATION_LONG)
        }
        var array = [String]()
        for module in modules {
            array.append(module.rawValue)
        }
        aCoder.encode(array, forKey: KEY_MODULES)
        
    
        
    }
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: KEY_NAME) as! String
        id = aDecoder.decodeObject(forKey: KEY_ID) as! String
        if let longObj =  aDecoder.decodeObject(forKey: KEY_LOCATION_LONG) {
            location?.longitude = longObj as! CLLocationDegrees
        }
        if let latiObj =  aDecoder.decodeObject(forKey: KEY_LOCATION_LATI) {
            location?.latitude = latiObj as! CLLocationDegrees
        }
        
        let modulesArray = aDecoder.decodeObject(forKey: KEY_MODULES) as! [String]
        for module in modulesArray {
            modules.insert(DeviceModel.Modules(rawValue: module)!)
        }
        
        
    }
    
}



