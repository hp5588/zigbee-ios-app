//
//  InfoView.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/30.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit

class InfoView: UIView {

    
//     Only override draw() if you perform custom drawing.
//     An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//    
//    }
    
    
//    private let parentFrame:CGRect?
//    private var myFrame:CGRect?
    
    private let size = 100
    private let centerFrame : CGRect?
    let title : String = "Title"
    let subTitle : String = "subtitle"
    

    init(parentFrame : CGRect) {
        centerFrame = CGRect(x: Int(parentFrame.width/2) - size/2, y: Int(parentFrame.height/2) - size/2, width: size, height: size)
        super.init(frame : centerFrame!)
        
        //customize view
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        self.layer.cornerRadius = 20
        
        let label : UILabel?
        label = UILabel(frame: CGRect(x: 12, y: 8, width: self.frame.size.width-90, height: 50))
        label?.text = title
        label?.textColor = UIColor.white
        label?.numberOfLines = 0
        label?.font = UIFont.systemFont(ofSize: 14)
        self.addSubview(label!)

    }
    
//    func putCenter() {
//        self.frame = CGRect(x: Int(self.center.x) - size/2, y: Int(self.center.y) - size/2, width: size, height: size)
//    }
//    
    
//    override init (frame : CGRect) {
//        let frame = CGRect(x: 20, y: 20, width: 100, height: 100)
//        super.init(frame : frame)
//        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
//        self.layer.cornerRadius = 20
//        
        
        
//        label = UILabel(frame: CGRect(x: 12, y: 8, width: self.frame.size.width-90, height: 50))
//        label.text = "Connection error please try again later!!"
//        label.textColor = UIColor.whiteColor()
//        label.numberOfLines = 0
//        label.font = UIFont.systemFontOfSize(14)
//        self.addSubview(label)
//
//        button = UIButton(frame: CGRect(x: self.frame.size.width-87, y: 8, width: 86, height: 50))
//        button.setTitle("OK", forState: UIControlState.Normal)
//        button.setTitleColor(UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: 1.0), forState: UIControlState.Normal)
//        button.addTarget(self, action: "hideSnackBar:", forControlEvents: UIControlEvents.TouchUpInside)
//        self.addSubview(button)
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
//    var title : String?
//    
//    
//    init(title:String = "Info") {
//        self.title = title
//        
//        
//    }
//    func test() {
//        
//    }
}
