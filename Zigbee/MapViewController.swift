//
//  MapViewController.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/9/2.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
//import MapKit
import GoogleMaps
//import GooglePlaces
import SwiftLocation

protocol MapViewDelegate {
    func mapViewWillReturn(location: CLLocationCoordinate2D)
}


class MapViewController: UIViewController{
    
    
    var delegate: MapViewDelegate?

    internal var mapView : GMSMapView?
    internal var chosenLocation: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //add finish button
        let finishButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector (finishButtonPress(sender:)))
        finishButton.title = "Finish"

        navigationItem.rightBarButtonItem = finishButton
        
            // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func loadView() {
        //23.051388, 120.176612
        var camera : GMSCameraPosition?
        camera = GMSCameraPosition.camera(withLatitude:(chosenLocation?.latitude)!, longitude:(chosenLocation?.longitude)!, zoom:16)

        mapView = GMSMapView.map(withFrame: .zero, camera: camera!)
        mapView?.isMyLocationEnabled = true
        mapView?.settings.compassButton = true
        mapView?.settings.myLocationButton = true
        mapView?.delegate = self
        
        addMarker(location: chosenLocation!)

        view = mapView
    }

    func setInitLocation(location : CLLocationCoordinate2D){
        chosenLocation = location
    }

}

extension MapViewController{
    //buttons callbacks
    func finishButtonPress(sender:UIButton){
        if chosenLocation != nil {
            print("location chosen")
            delegate?.mapViewWillReturn(location: chosenLocation!)
            self.navigationController?.popViewController(animated: true)
        }
    }
}


extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView:GMSMapView, didTapAt coordinate:CLLocationCoordinate2D) {
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        
        
//        if chosenLocation != nil {
//            mapView.clear()
//            chosenLocation = nil
//        }else{
//            addMarker(location: coordinate)
//            
//            //update choosen location variable
//            chosenLocation = coordinate
//        }

    }
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        print("You long press at \(coordinate.latitude), \(coordinate.longitude)")
        mapView.clear()
        addMarker(location: coordinate)
        chosenLocation = coordinate

    }
}

extension MapViewController{
    func addMarker(location : CLLocationCoordinate2D){
        let marker = GMSMarker(position: location);
        marker.title = "Trigger Range"
        marker.snippet = "set the range to trigger notification"
        marker.tracksInfoWindowChanges = true
        marker.map = self.mapView


        //draw range on the map with tip point as center
        let rangeCircle  = GMSCircle(position: location, radius: 300)
        rangeCircle.strokeColor = UIColor(hexString: "#008BFFFF")
        rangeCircle.fillColor   = UIColor(hexString: "#008BFF50")

        rangeCircle.map = self.mapView
    }
}
