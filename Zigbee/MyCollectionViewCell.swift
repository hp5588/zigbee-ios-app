//
//  MyCollectionViewCell.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/6.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
import BAFluidView

class MyCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var subTextLabel: UILabel!
    
    private var fluidView : BAFluidView!
    
    var device: DeviceModel!{
        didSet{
            if device.modules.contains(.WaterLevel) {
                if fluidView == nil {
                    var frame = self.frame
                    frame.origin = CGPoint(x: 0, y: 0)
                    fluidView = BAFluidView(frame : frame)
                    fluidView.fillColor = UIColor(hexString : "#2088B68F")
                    fluidView.fillRepeatCount = 1
                    fluidView.fillAutoReverse = false;
                    fluidView.maxAmplitude = 5
                    fluidView.layer.zPosition = 10
            
                    super.addSubview(fluidView)
                    
                    //bring text to the front
                    textLabel.layer.zPosition = 5
                    subTextLabel.layer.zPosition = 5
                }
            }
            updateCell()
        }
    }
    

    
    func updateCell() {
        textLabel.text = device.name
        subTextLabel.text = device.id
        if fluidView != nil {
            fluidView.fill(to: device.property.waterLevel! as NSNumber)
            fluidView.startAnimation()
        }
    }
    
    
    
    
    
    
    
}
