//
//  NotificationManager.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/9/5.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
import UserNotifications
class NotificationManager: NSObject {
    static let shared : NotificationManager = NotificationManager()
    
    private let notificationCenter = UNUserNotificationCenter.current()
    internal let inRangeIdentifier = "in-range-identifier"
    
    internal let notificationCategoryIdentifier = "category-identifier"
    internal let confirmActionIdentifier = "confirm-action-identifier"

    
    override init() {
        super.init()
        notificationCenter.delegate = self
        
        //init stadard notification
        
        
    }
    
    func requestAuth() {
        notificationCenter.requestAuthorization(options: [.alert,.sound], completionHandler: {granted, error in
            if (granted){
                print("authorized")
            }else{
                print(error.debugDescription)
            }
        })
    }
    func notifyInRange(device : DeviceModel) {
        let req = getNotificationRequest(title: "Door In Range",
                                         body: "Do you want to open the door named [" + device.name + "]")
//        let categories : Set<UNNotificationCategory> = [getNotificationCatagory()]
        notificationCenter.setNotificationCategories([getNotificationCatagory()])
        notificationCenter.add(req, withCompletionHandler: nil)
    }
    
    
    
}

//notification contructing method
extension NotificationManager{
    
    internal func getNotificationRequest(title: String, body : String) -> UNNotificationRequest {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = notificationCategoryIdentifier

        let req = UNNotificationRequest(identifier: inRangeIdentifier, content: content, trigger: UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false))
        return req
    }
    
    internal func getNotificationCatagory() -> UNNotificationCategory{
        let confirmAction = UNNotificationAction(identifier: confirmActionIdentifier, title: "Unlock Door", options: [.authenticationRequired])

        let catagory = UNNotificationCategory(identifier: notificationCategoryIdentifier, actions: [confirmAction], intentIdentifiers: [], options: [])
        
        return catagory
    }
  
}

extension NotificationManager : UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
}
