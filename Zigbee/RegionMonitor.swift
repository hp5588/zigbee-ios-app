//
//  RegionMonitor.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/9/4.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit
import SwiftLocation
import MapKit



protocol RegionDelegate {
    func inRange(device : DeviceModel)
    func outRange(device : DeviceModel)
}

class RegionMonitor: NSObject {
    
    
    static let shared : RegionMonitor = RegionMonitor()
    
    var delegate : RegionMonitor?
    private var dataManager = DataManager.shared
    
    
    override init() {
    
    }
    
    func monitorDeviecs(deviceList : [DeviceModel]){
        
        for device in deviceList {
            if device.modules.contains(DeviceModel.Modules.DoorLock) {
                let cirRegion = CLCircularRegion(center: device.location!, radius: 300, identifier: device.id)
                
                do{
                    let req = try Location.monitor(region: cirRegion, enter: {_ in
                        print("inside region of " + device.id)
                        NotificationManager.shared.notifyInRange(device: device)
                    }, exit: {_ in
                        print("out region of " + device.id)
                        
                    }, error: {req, error in
                        print(error)
                    })
                    
                    print(req.description)
                }catch{
                    print(error)
                }
            }
            
        }
    }
    
    func updateMonitorDeviceFromDataManager() {
        monitorDeviecs(deviceList: dataManager.getDeviceList())
    }
    
    
}
