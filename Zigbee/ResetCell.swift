//
//  ResetCell.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/17.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit


protocol CellDelegate {
    func resetButtonClicked(button:UIButton)
}


class ResetCell: UITableViewCell {

    @IBOutlet weak var resetButton: UIButton!
    
    var delegate : CellDelegate!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        resetButton.addTarget(self, action: #selector(reset(sender:)), for: .touchUpInside)
    }
    
    func reset(sender:UIButton!){
        if delegate != nil {
            delegate.resetButtonClicked(button: sender)
        }
        
    }
    
    

}
