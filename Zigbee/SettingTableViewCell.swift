//
//  SettingTableViewCell.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/7.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit


protocol SettingActionDelegate {
    func buttonClicked(button:UIButton)
}

class SettingTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var optionSwitch: UISwitch!
    
    var delegate : SettingActionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        optionSwitch.addTarget(self, action: #selector(buttonClick(sender:)), for: .touchUpInside)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func buttonClick(sender:UIButton!) {
        delegate?.buttonClicked(button: sender)
    }
    


}
