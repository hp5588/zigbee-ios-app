//
//  SwitchCell.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/12.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit



class SwitchCell: CellBase {

    @IBOutlet weak var stateSwitch: UISwitch!

    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func updateCell(){
        super.device.property.switchState == DeviceModel.Switch.On ? stateSwitch.setOn(true, animated: true) : stateSwitch.setOn(false, animated: true)
    }


}
