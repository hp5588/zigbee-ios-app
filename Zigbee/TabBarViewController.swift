//
//  TabBarViewController.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/7.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let collectionViewController =  self.viewControllers?[0] as! CollectionViewCotroller
        let settingViewController =  self.viewControllers?[1] as! SettingTableViewController
        
        collectionViewController.settingController = settingViewController
        
        UITabBar.appearance().barTintColor = .black
        UITabBar.appearance().tintColor = UIColor.white
//        UITabBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
