//
//  TemperatureCell.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/12.
//  Copyright © 2017年 高 伯文. All rights reserved.
//

import UIKit

class TemperatureCell: CellBase {

    @IBOutlet weak var temperatureLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func updateCell() {
        temperatureLabel.text = String(device.property.temperature)
    }

}
