//
//  WaterLevelCell.swift
//  Zigbee
//
//  Created by 高 伯文 on 2017/8/13.
//  Copyright © 2017年 高 伯文. All rights reserved.
//



import BAFluidView
import UIKit

class WaterLevelCell: CellBase {
    @IBOutlet weak var percentageLabel: UILabel!

    var fluidView:BAFluidView!

    override func updateCell() {
        if fluidView == nil {
            fluidView = BAFluidView(frame : self.frame)
            fluidView.fillColor = UIColor(hexString : "#2088B6AF")
            fluidView.fillRepeatCount = 1
            fluidView.fillAutoReverse = false;
            super.addSubview(fluidView)
        }
        fluidView.fill(to: device.property.waterLevel! as NSNumber)
        percentageLabel.text = String(device.property.waterLevel*100)
//        fluidView.fill(to: 0.5)

        fluidView.startAnimation()
        
    }

}


extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.characters.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}
